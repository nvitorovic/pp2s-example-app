#include "primer.h"
#include <stdlib.h>

int add(int x, int z){
	return x+z;
}

int sub(int x, int y){
	return x-y;
}

int* addarr(const int* x, const int* y, int n){
	int* ret = NULL;
	int i;
	if(n<=0){
		return NULL;
	}
	ret = (int*) calloc(sizeof(int), n);
	for(i=0; i<n; i++){
		ret[i] = add(x[i], y[i]);
	}
	return ret;
}

void main(){
	int niz[]={5, 6, 7, 8};
	int* xx = (int*) calloc(sizeof(int), 4);
	int i;
	for(i=0; i<4; i++) xx[i]=i+1;
	addarr(niz, niz, 4);
	addarr(xx, xx, 4);
}