#ifndef _primer_h_
#define _primer_h_

/*
Ova funkcija obavlja sabiranje dva cela broja
*/
int add(int, int);

/*
Ova funkcija obavlja oduzimanje dva cela broja
*/
int sub(int, int);

/*
Ova funkcija obavlja sabirnaje dva vektora duzina n.
*/
int* addarr(const int*, const int*, int n);

#endif